window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

    const loadingIcon = document.getElementById('loading-conference-spinner')
    loadingIcon.classList.add('d-none')
    selectTag.classList.remove('d-none')

    }
    // get the attendee form element by it's id
    const formTag = document.getElementById('create-attendee-form')
    // add an event handler for the submit event
    formTag.addEventListener('submit', async event => {
        // prevent the default from happening
        event.preventDefault();
        // create a FormData object from the form
        const formData = new FormData(formTag);
        // get a new object from the form data's entries
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        // create options for the fetch
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        // make the fetch using the await keyword and attendee URL
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newAttendee = await response.json();
            console.log(newAttendee)
        }
        formTag.classList.add('d-none')
        const sucessAlert = document.getElementById('success-message')
        sucessAlert.classList.remove('d-none')

    })

  });
