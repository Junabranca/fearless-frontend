function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitule mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer" <time>${startDate} - ${endDate}</time></div>
        </div>
      </div>`
}


function placeholder(data){
        return `
        <div class="card" aria-hidden="true">
        <img src="..." class="card-img-top" alt="...">
          <div class="card-body">
          <h5 class="card-title placeholder-glow">
            <span class="placeholder col-6"></span>
          </h5>
          <p class="card-text placeholder-glow">
            <span class="placeholder col-7"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-6"></span>
            <span class="placeholder col-8"></span>
          </p>
        </div>
      </div>
      `;
  }

function createAlert() {
  return `
  <div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Looks like that's a bad response!</h4>
      <p></p>
      <p>Got nothing here for ya, sorry.</p>
      <hr>
      <p class="mb-0">If you wanna keep looking at this error, you're more than welcome to.</p>
  </div>
  `
}
console.log(alert)

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col')
    let colIndx = 0

    try{
    const response = await fetch(url);

    if (!response.ok){
        const column = document.querySelector('.col')
        column.innerHTML += createAlert();
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const waiting = placeholder(details);
              column.innerHTML += waiting;
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const startDate = new Date(details.conference.starts).toLocaleDateString();
              const endDate = new Date(details.conference.ends).toLocaleDateString();
              const location = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, startDate, endDate, location);
              const column = columns[colIndx % 3];
              column.innerHTML += html;
              colIndx = (colIndx + 1) % 3;

        }
    }

   }
} catch (e) {
    console.error('error', e)

}
});
